const container = document.createElement("div");
container.style.display = "flex";
container.style.flexWrap = "wrap";
container.style.width = "1100px";
container.style.margin = "auto";

const button = document.createElement("button");
button.textContent = "намалювати коло";
document.body.appendChild(button);

button.addEventListener("click", () => {
  const input = document.createElement("input");
  input.type = "number";
  input.min = 10;
  input.max = 200;
  input.value = 100;
  input.style.marginTop = "10px";
  input.style.marginBottom = "10px";
  document.body.appendChild(input);

  const drawButton = document.createElement("button");
  drawButton.textContent = "< діаметр";
  drawButton.style.marginBottom = "10px";
  document.body.appendChild(drawButton);

  drawButton.addEventListener("click", () => {
    container.innerHTML = "";
    const diameter = input.value;

    for (let i = 0; i < 100; i++) {
      const circle = document.createElement("div");
      circle.style.width = diameter + "px";
      circle.style.height = diameter + "px";
      circle.style.borderRadius = "50%";
      circle.style.margin = "5px";
      circle.style.backgroundColor = getRandomColor();
      circle.addEventListener("click", () => {
        container.removeChild(circle);
      });
      container.appendChild(circle);
    }
  });
});

function getRandomColor() {
  const letters = "0123456789ABCDEF";
  let color = "#";
  for (let i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}

document.body.appendChild(container);
